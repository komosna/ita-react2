const express = require('express')
const cors = require('cors')
const jsonServer = require('json-server')
const app = express()
const bodyParser = require('body-parser');
const session = require('express-session');
const fs = require('fs');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const jwtDecode = require('jwt-decode');
const path = require('path');
const packageJson = require('./package.json');

const mailing = require('./mailing');
const auditLog = require('./audit-log');

const JWT_SECRET = 'dfa6sd4f6a5sd4f684e33f6856a5ds4f6s5a4f6sad';

let USERS = {};


function saveUserPasswords() {
  let db = JSON.parse(fs.readFileSync('db.json'));
  let dbUsers = (db.users || [])
  let authUsers = getUsers();
  for(let user of dbUsers) {
    let authUserIndex = authUsers.findIndex(u => u.name === user.name)
    if(authUserIndex > -1) {
      user.password = authUsers[authUserIndex].password
    }
  }
  fs.writeFileSync('db.json', JSON.stringify(db, null, 2))
}

function loadUsers() {
  setUsers(JSON.parse(fs.readFileSync('db.json')).users || []);
}

function getUsers() {
  return USERS;
}

function setUsers(users) {
  USERS = users;
}

function getUser(userName) {
  return getUsers().find(u => u.login === userName) || {};
}

function setPassword(userName, password) {
  getUser(userName).password = bcrypt.hashSync(password, 10);
  saveUserPasswords();
}

function getPassword(userName) {
  return getUser(userName).password;
}

function passwordCompareSync(password, passwordHash) {
  if(passwordHash.startsWith('PLAINTEXT=')) {
    return passwordHash.slice(10) === password;
  } else {
    return bcrypt.compareSync(password, passwordHash);
  }
}

function isAdmin(userName) {
  return getUser(userName).isAdmin;
}

function isLoggedIn(req) {
  return req.session.loggedUserName !== undefined;
}

function isLoggedAdmin(req) {
  return isLoggedIn(req) && isAdmin(req.session.loggedUserName);
}

// Serve static files from the React app
app.use(express.static(path.join(__dirname, 'client/build')));

app.use(cors({
  credentials: true,
  origin: function (origin, callback) {
    // TODO: Solve for prod/dev environments so it restricts to allowed origins only!
    if (true) {
      callback(null, true);
    } else {
      callback(new Error('Not allowed by CORS'));
    }
  }
}));
app.use(session({
  secret: 'sad658f4ds65fd4168S4F1D6w41d9f8ew41f6ds5f14684FD16S',
  resave: false,
  saveUninitialized: true,
  cookie: { secure: false }
}));
app.use(bodyParser.json());
app.use(function(req, res, next) {
  console.log('>', req.method, req.originalUrl)
  loadUsers();
  next();
})

app.use(auditLog.middleware);

app.get('/version', function(req, res) {
  res.send({version: packageJson.version});
})

app.post('/login', function(req, res) {
  let username = req.body.email;
  let password = req.body.password;
  if(getUser(username).login && passwordCompareSync(password, getUser(username).password)) {
    req.session.loggedUserName = username;
    res.send({status: 'OK'});
    auditLog.log({user: username, operation: 'login'});
  } else {
    res.status(403);
    res.send({status: 'Unknown user or incorrect password.'});
  }
});

app.post('/logout', function(req, res) {
  req.session.loggedUserName = undefined;
  res.send();
})

app.get('/user', function(req, res) {
  if(!isLoggedIn(req)) {
    res.send(JSON.stringify(null));
    return;
  }
  res.send({
    username: req.session.loggedUserName,
    name: getUser(req.session.loggedUserName).name,
    isAdmin: isAdmin(req.session.loggedUserName),
  });
})

app.post('/changePassword', function(req, res) {
  let username = req.body.username || req.session.loggedUserName;
  if(isLoggedAdmin(req)) {
    if(passwordCompareSync(req.body.oldPassword, getUser(req.session.loggedUserName).password)) {
      setPassword(username, req.body.newPassword);
      res.send({status: "OK"});
    } else {
      res.status(403);
      res.send({status: "Invalid old password."});
    }
  } else if(isLoggedIn(req)) {
    if(passwordCompareSync(req.body.oldPassword, getUser(req.session.loggedUserName).password)) {
      if(req.session.loggedUserName === username) {
        setPassword(req.session.loggedUserName, req.body.newPassword);
        res.send({status: "OK"});
      } else {
        res.status(403);
        res.send({status: "Non-admin users can only change password to themselves."})
      }
    } else {
      res.status(403);
      res.send({status: "Invalid old password."});
    }
  } else {
    res.status(403);
    res.send({status: "You need to be logged in to change the password."});
  }
})



function validateEmail(email) {
  var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(email.toLowerCase());
}


app.post('/requestPasswordRecovery', async function(req, res) {
  const userName = req.body.username;
  if(!validateEmail(userName)) {
    res.status(400);
    res.send({
      status: "Provided user name is not an email address.",
    });
    return;
  }
  if(!getUser(userName).login) {
    // This is intentional so nobody can determine which addresses are registered.
    setTimeout(() => {
      res.send({
        status: 'OK',
      });
    }, 500);
    return;
  }
  let token = jwt.sign(
    {
      username: userName,
      hashHash: bcrypt.hashSync(getUser(userName).password, 10),
    }, 
    JWT_SECRET, 
    {
      expiresIn: 24*60*60
    }
  );
  let resetLink = `${req.get('Origin')}/#/reset-password/?token=${token}`;
  try {
    await mailing.sendPasswordRecoveryEmail(userName, resetLink);
  } catch(e) {
    console.log(e);
    res.status(500);
    res.send({
      status: "Sending of the email failed."
    })
    return;
  }
  res.send({
    status: 'OK',
  });
});



app.post('/resetPassword', function(req, res) {
  const {checkOnly} = req.query;
  const {token, newPassword} = req.body;
  let verifiedToken;
  try {
    verifiedToken = jwt.verify(token, JWT_SECRET, {algorithms: ['HS256']});
  } catch(e) {
    console.error(e);
    res.status(400);
    res.send({
      status: "The token could not be verified."
    })
    return;
  }
  if(!verifiedToken) {
    res.status(400);
    res.send({
      status: "Invalid token. It is either expired or corrupted."
    })  
    return;  
  }
  
  if(!bcrypt.compareSync(getUser(verifiedToken.username).password, verifiedToken.hashHash)) {
    res.status(400);
    res.send({
      status: "The token was already used for password change or the password was changed by a different way."
    })  
    return;      
  }

  if(!checkOnly) {
    setPassword(verifiedToken.username, newPassword)
    res.send(Object.assign({},
      verifiedToken,
      {status: "The password has been successfully set."}
    ));
    return;
  }  

  res.send(Object.assign({},
    verifiedToken,
    {status: "The token is valid."}
  ));
})



const router = jsonServer.router('db.json')
app.use(jsonServer.bodyParser)
app.use(router)


const port = process.env.PORT || 8081

app.listen(port)
