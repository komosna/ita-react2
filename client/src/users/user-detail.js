import React from 'react'
import { Link } from 'react-router-dom'
import ROUTES from '../routes'

import usersService from './users-service'

class UserDetail extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      user1: {}
    }
  }

  componentWillMount() {
    this.load(this.props.match.params.id)
  }

  componentWillReceiveProps(newProps) {
    if (this.props.match.params.id !== newProps.match.params.id) {
      this.load(newProps.match.params.id)
    }
    console.log(newProps)
  }

  async load(id) {
    const res = await usersService.getUser(id)

    this.setState({
      user1: res.data
    })
  }

  render() {
    const user1 = this.state.user1

    return (
      
      <div className="box box-default">
        <div className="box-header">
          <h3 className="box-title">User Detail</h3>
        </div>
        <div className="box-body">     
          <div className="col-md-12">
            <div className="btn-group with-border margin-bottom">
              <Link className="btn btn-default" to={ROUTES.getUrl(ROUTES.USER_EDIT, { id: user1.id })}>Edit</Link>
              <Link className="btn btn-default" to={ROUTES.getUrl(ROUTES.USER_PASSWORD_CHANGE, {id: user1.id})}>Change Password</Link>
              <Link className="btn btn-default" to={ROUTES.getUrl(ROUTES.USER_LISTING)}>Delete</Link>
            </div>
            <table className="table table-borderless margin-bottom">
              <tbody>
                <tr>
                  <th className="col-md-1 text-right">Login</th>
                  <td className="col-md-11">{user1.login}</td>
                </tr>
                <tr>
                  <th className="text-right">Name</th>
                  <td>{user1.name}</td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    )
  }
}


export default UserDetail