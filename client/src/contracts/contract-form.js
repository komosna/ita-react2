import React from "react"
import { price, dateToLocal } from "../format"

const ContractForm = ({ contract, customers, statuses, handleChange }) => {
  return (
    <form>
      <div className="row">
        <div className="form-group col-md-4">
          <label className="col-form-label">Name</label>
          <input
            name="name"
            type="text"
            className="form-control"
            value={contract.name}
            onChange={handleChange}
          />
        </div>
        <div className="form-group col-md-4">
          <label className="col-form-label">Price</label>
          <input
            name="price"
            type="text"
            className="form-control currency"
            value={price(contract.price)}
            onChange={handleChange}
          />
        </div>
        <div className="form-group col-md-2">
          <label className="col-form-label">Customer</label>
          <select
            className="form-control"
            name="customerId"
            value={contract.customerId}
            onChange={handleChange}
          >
            <option value="">Select...</option>
            {customers.map(c => (
              <option key={c.id} value={c.id}>
                {c.name}
              </option>
            ))}
          </select>
        </div>
        <div className="form-group col-md-4">
          <label className="col-form-label">Deadline</label>
          <input
            name="deadline"
            type="text"
            placeholder="YYYY-MM-DD"
            className="form-control"
            value={contract.deadline ? contract.deadline.split("T")[0] : ""}
            onChange={handleChange}
          />
        </div>
        <div className="form-group col-md-2">
          <label className="col-form-label">Status</label>
          <select
            className="form-control"
            name="status"
            value={contract.status}
            onChange={handleChange}
          >
            <option value="">Select...</option>
            {statuses.map(s => (
              <option key={s.status} value={s.status}>
                {s.status}
              </option>
            ))}
          </select>
        </div>
      </div>
      <div className="row">
        <div className="form-group col-md-12">
          <label className="col-form-label">Description</label>
          <textarea
            name="description"
            placeholder="Description"
            className="form-control"
            value={contract.description}
            onChange={handleChange}
          />
        </div>
      </div>
    </form>
  )
}

export default ContractForm
