const priceFormat = new Intl.NumberFormat("cs-CS", {
  style: "currency",
  currency: "CZK"
})

export const price = v => (isNaN(v) ? v : priceFormat.format(v))

export const dateToLocal = v => new Date(v).toLocaleDateString("sv-se")

export const dateToIsoString = v => new Date().toISOString()
